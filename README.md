# README - TECRM Source Code #

This folder contains all the TECRM source code and files provided by CloudCover on 02/02/2021.

Notes on setting this up can be found [here on Confluence](https://clearskylogic.atlassian.net/wiki/spaces/TE/pages/1875345409/TECRM+Configuration+and+ODBC+Connection).

It takes the form of an Access front end that uses the TOG DB server for its backend (so use the username and passwords for those). There are references to yet another API in this code which we have never found.

# REMEMBER IT WILL BE TALKING TO THE LIVE DB AS THERE IS NO DEV ENVIRONMENT FOR THIS!!! #
### BE VERY, VERY CAREFUL RUNNING (OPENING) THIS APPLICATION! ###

We currently do not know, and are not sure, whether this is the 'only' version (there seems to be at least two in here)...tread with care, and be careful with assumptions...

